function! deleteExtraSpaces#deleteExtraHorizontalSpaces()
  let l:extraWhitespace = search("\\s\\s\\+", "czW")
  if (l:extraWhitespace != 0)
    execute "normal ciw\<space>"
  endif
endfunction

function! deleteExtraSpaces#deleteAllHorizontalSpaces()
  let l:whitespace = search("\\s\\+", "czW")

  if (l:whitespace != 0)
    execute "normal diw"
  endif
endfunction

function! deleteExtraSpaces#deleteExtraVerticalSpaces()
  call deleteExtraSpaces#deleteVerticalSpacesHelper(1)
endfunction

function! deleteExtraSpaces#deleteAllVerticalSpaces()
  call deleteExtraSpaces#deleteVerticalSpacesHelper(0)
endfunction

function! deleteExtraSpaces#deleteVerticalSpacesHelper(linesToLeave)
  let l:begin = search("^[^\\n].*$\\n\\n", "cbW")
  if (getline(l:begin) !~ "^\\s*$" )
    let l:begin += 1
  endif
  let l:end = search("^\\s*\\n[^\\n]", "Wn") - a:linesToLeave

  if (l:end == 0) "this happens when we hit eof, so we set it to the bottom of the file
    let l:end = line("$")
  endif

  if (l:end >= l:begin)
    let l:delta = (l:end - l:begin) + 1
    call cursor(l:begin, 0)
    exec "normal " . l:delta . "dd"
  endif
endfunction

function! deleteExtraSpaces#callRelevantDelete(VerticalFunc, HorizontalFunc)
  let l:currentline = getline(".")
  if (empty(l:currentline)) || (l:currentline =~ "^\\s\\+$")
    call a:VerticalFunc()
  else
    call a:HorizontalFunc()
  endif
endfunction

function! deleteExtraSpaces#da()
  call deleteExtraSpaces#callRelevantDelete(
        \ function("deleteExtraSpaces#deleteAllVerticalSpaces"),
        \ function("deleteExtraSpaces#deleteAllHorizontalSpaces"))
endfunction

function! deleteExtraSpaces#di()
  call deleteExtraSpaces#callRelevantDelete(
        \ function("deleteExtraSpaces#deleteExtraVerticalSpaces"),
        \ function("deleteExtraSpaces#deleteExtraHorizontalSpaces"))
endfunction
