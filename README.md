This is a vim plugin to delete extra horizontal and vertical spaces between two
blocks of text.

# Usage:

There are two mappings:

1. di<space> : delete extra spaces (leaves 1 space)
1. da<space> : delete all spaces   (leaves 0 space)

Depending on the contents of the line your cursor is on, the plugin will
determine whether to delete vertical spaces or horizontal spaces.
