nnoremap <silent> <Plug>(DeleteExtraSpaces-DeleteInnerSpace) :call deleteExtraSpaces#di()<cr>:silent! call repeat#set("\<Plug>(DeleteExtraSpaces-DeleteInnerSpace)", v:count)<cr>
nnoremap <silent> <Plug>(DeleteExtraSpaces-DeleteASpace) :call deleteExtraSpaces#da()<cr>:silent! call repeat#set("\<Plug>(DeleteExtraSpaces-DeleteASpace)", v:count)<cr>

nnoremap <silent> di<space> <Plug>(DeleteExtraSpaces-DeleteInnerSpace)
nnoremap <silent> da<space> <Plug>(DeleteExtraSpaces-DeleteASpace)
